@echo off

echo "Registrate user"
d:\usr\curl\bin\\curl -X POST -d "userid=pupkin&passwd=1&email=ppkn@dot.com" 127.0.0.1:8888/users/registration
echo ""
echo "Get info about user"
d:\usr\curl\bin\\curl -u pupkin:1 -X GET 127.0.0.1:8888/users/about
echo ""
echo "Create short url"
d:\usr\curl\bin\\curl -u pupkin:1 -X POST -d "src=https://stulchik.com/yadivan.htm" 127.0.0.1:8888/shorturls/create
echo ""
echo "Get owner urls"
d:\usr\curl\bin\\curl -u pupkin:1 -X GET "127.0.0.1:8888/shorturls/owned?start=0&limit=1"

pause
