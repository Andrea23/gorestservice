
Что бы собрать и запустить проект, нужно:

- скачать исходники и переметить их в GOROOT по пути: src\github.com\andrea2312\gorestservice, где gorestservice конечная папка с исходниками

- скачать пакет драйвера для MySQL: "github.com/go-sql-driver/mysql"

- настроить подключение к MySQL серверу, в gorestservice\config\values.go .
Задать значения для:

	MySQLServer = <адрес сервера>(например: "tcp(127.0.0.1:3306)" )
	MySQLDB = <название БД> (например: "gorestservice?parseTime=true") Параметр parseTime=true в подключении обязателен!
	MySQLUser = <логин к БД>
	MySQLPasswd = <парольк к БД>

- создать БД под названием "gorestservice"

- создать таблицы через SQL в БД, в сторогом порядке:
1 - gorestservice\sql_queries\create_table_users.sql
2 - gorestservice\sql_queries\create_table_shorturls.sql
3 - gorestservice\sql_queries\create_table_referers.sql

Перейти в папку gorestservice и собрать проект.

Адреса и параметры запросов REST сервиса:

Регистрация пользователя:
URL: users/registrate
Метод: POST
Параметры:
- userid - имя пользователя
- passwd - пароль
- email - E-Mail

Получение информации о текущем авторизованном пользователе:
URL: users/about
Метод: GET
Пользователь обязательно должен быть авторизован!

создание новой короткой ссылки :
URL: shorturls/create
Метод: POST
Параметры:
- src - исходная ссылка
Пользователь обязательно должен быть авторизован!

получение всех созданных коротких ссылок пользователя:
URL: shorturls/owned
Метод: GET
Пользователь обязательно должен быть авторизован!

получение информации о конкретной короткой ссылке пользователя:
URL: shorturls/about?src=
Метод: GET
Параметры:
- src - короткая ссылка

удаление короткой ссылки пользователя:
URL: shorturls/delete/<Id>
Метод: DELETE
Параметры: 
- id - Номер короткой ссылки

Получение редиректа на полный url по короткой ссылке:
URL: shorturls/reverse 
Метод: PUT
Параметры:
- передать короткую ссылку в body запроса

получение топа из 20 сайтов иcточников переходов:
URL: shorturls/stat?src=
Метод: GET
Параметры:
- src - короткая ссылка


!!! Протестировать проект можно через утилиту curl (https://curl.haxx.se/).
Пример запросов ниже. Далее адрес 127.0.0.1:8888 указывает на адрес по которому доступен запущенный сервис.

Регистрация пользователя:
curl -X POST -d "userid=pupkin&passwd=1&email=ppkn@dot.com" 127.0.0.1:8888/users/registration
 
Получение информации о текущем авторизованном пользователе:
curl -u pupkin:1 -X GET 127.0.0.1:8888/users/about

создание новой короткой ссылки :
curl -u pupkin:1 -X POST -d "src=https://stulchik.com/yadivan.htm" 127.0.0.1:8888/shorturls/create

получение всех созданных коротких ссылок пользователя:
curl -u pupkin:1 -X GET 127.0.0.1:8888/shorturls/owned

получение информации о конкретной короткой ссылке пользователя:
curl -u pupkin:1 -X GET 127.0.0.1:8888/shorturls/about?src=<короткая ссылка>

Получение редиректа на полный url по короткой ссылке:
curl -u pupkin:1 -X PUT -d "<короткая ссылка>" 127.0.0.1:8888/shorturls/reverse
 
получение топа из 20 сайтов иcточников переходов:
curl -u pupkin:1 -X GET 127.0.0.1:8888/shorturls/stat?src=<короткая ссылка>

удаление короткой ссылки пользователя:
curl -u pupkin:1 -X DELETE 127.0.0.1:8888/shorturls/delete/<ID>
