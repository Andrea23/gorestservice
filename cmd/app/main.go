package main

import (
	//"fmt"
	"log"

	"bitbucket.org/Andrea23/gorestservice/internal/config"
	"bitbucket.org/Andrea23/gorestservice/internal/server"
)

func main() {

	config, err := config.ReadFromFile("./configs/config_prod.json")
	if err != nil {
		log.Fatalf("Cannot create Config from file: %s", err)
	}

	app := server.NewApp(config)

	if err := app.Run("8888"); err != nil {
		log.Fatalf("%s", err.Error())
	}
}
