module bitbucket.org/Andrea23/gorestservice

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	golang.org/x/crypto v0.0.0-20211202192323-5770296d904e
)
