CREATE TABLE `shorturls` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`ownerid` INT NOT NULL,
	`createdate` DATE NOT NULL,
	`originalurl` VARCHAR(256) NOT NULL,
	`newurl` VARCHAR(256) NOT NULL,
	`visits` INT DEFAULT 0,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`ownerid`) REFERENCES users(id)
);