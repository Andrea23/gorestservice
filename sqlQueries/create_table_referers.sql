CREATE TABLE `referers` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`shorturlid` INT NOT NULL,
	`source` VARCHAR(256) NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`shorturlid`) REFERENCES shorturls(id)
);