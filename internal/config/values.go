package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Values struct {
	UseInMemoryRepository bool `json:"UseInMemoryRepository"`

	MySQLServer string `json:"MySQLServer"`

	MySQLDB string `json:"MySQLDB"`

	MySQLUser   string `json:"MySQLUser"`
	MySQLPasswd string `json:"MySQLPasswd"`

	MySQLDBParams string `json:"MySQLDBParams"`

	MySQLConnectionString string
}

func ReadFromFile(filename string) (Values, error) {

	jsonFile, err := os.Open(filename)
	if err != nil {
		return Values{}, err
	}

	defer jsonFile.Close()
	byteValue, err := ioutil.ReadAll(jsonFile)

	if err != nil {
		return Values{}, err
	}

	var values Values
	err = json.Unmarshal(byteValue, &values)
	if err != nil {
		return Values{}, err
	}

	values.MySQLConnectionString = fmt.Sprintf("%s:%s@%s/%s%s", values.MySQLUser, values.MySQLPasswd, values.MySQLServer, values.MySQLDB, values.MySQLDBParams)
	return values, nil
}
