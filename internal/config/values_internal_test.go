package config

import (
	"testing"
)

func TestConfigReadFromFile(t *testing.T) {

	values, err := ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	if len(values.MySQLServer) == 0 || len(values.MySQLDB) == 0 ||
		len(values.MySQLUser) == 0 {
		t.Errorf("One of params missing")
	}
}
