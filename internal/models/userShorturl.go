package models

import (
	"errors"
	"time"
)

var (
	ErrNotFound   = errors.New("not found")
	ErrOutOfRange = errors.New("out of range")
)

type ServiceErrStatus int

const (
	StatusOK ServiceErrStatus = iota
	StatusInternalServiceError
	StatusServiceUnauthorized
)

type ServiceError struct {
	Err    error
	Status ServiceErrStatus
}

type User struct {
	ID       int
	Name     string
	Password string
	Email    string
}

type Shorturl struct {
	ID          int
	OwnerID     int
	TimeStamp   time.Time
	OriginalURL string
	NewURL      string
	Visits      int
	Referers    []string
}
