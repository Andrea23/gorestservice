package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/Andrea23/gorestservice/internal/config"
	"bitbucket.org/Andrea23/gorestservice/internal/handlers"
	"bitbucket.org/Andrea23/gorestservice/internal/interfaces"
	"bitbucket.org/Andrea23/gorestservice/internal/repository"
	"bitbucket.org/Andrea23/gorestservice/internal/services"
)

type App struct {
	httpServer *http.Server

	shorturlsrv services.ShortUrlService
	usersrv     services.UserService

	repo interfaces.BaseRepository
}

func NewApp(config config.Values) *App {

	baserepo, userrepo, err := repository.CreateUsersRepo(config)
	if err != nil {
		log.Fatalf("Cannot create Users repository: %s", err)
	}

	_, shorturlrepo, err := repository.CreateShortUrlsRepo(config)
	if err != nil {
		log.Fatalf("Cannot create ShortUrls repository: %s", err)
	}

	shorturlsrv := services.MakeShortUrlService(shorturlrepo)
	usersrv := services.MakeUserService(userrepo)

	return &App{
		shorturlsrv: shorturlsrv,
		usersrv:     usersrv,
		repo:        baserepo,
	}
}

func (a *App) Run(port string) error {

	// http server
	mux := http.NewServeMux()

	// Handlers
	// users
	mux.Handle("/users/registration", handlers.UsersRegistration(&a.usersrv))
	mux.Handle("/users/about", handlers.UsersAbout(&a.usersrv))
	// short urls
	mux.Handle("/shorturls/create", handlers.ShorturlsCreate(&a.shorturlsrv, &a.usersrv))
	mux.Handle("/shorturls/owned", handlers.ShorturlsOwned(&a.shorturlsrv, &a.usersrv))
	mux.Handle("/shorturls/about", handlers.ShorturlsAbout(&a.shorturlsrv, &a.usersrv))
	mux.Handle("/shorturls/delete", handlers.ShorturlsDelete(&a.shorturlsrv, &a.usersrv))
	mux.Handle("/shorturls/reverse", handlers.ShorturlsReverse(&a.shorturlsrv, &a.usersrv))
	mux.Handle("/shorturls/stat", handlers.ShorturlsStatistics(&a.shorturlsrv, &a.usersrv))

	// HTTP Server
	a.httpServer = &http.Server{
		Addr:    ":" + port,
		Handler: mux,
	}

	// Serve
	go func() {
		if err := a.httpServer.ListenAndServe(); err != nil {
			log.Fatalf("Failed to listen and serve: %+v", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Interrupt)

	<-quit

	ctx, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	if a.repo != nil {
		a.repo.Close()
	}

	return a.httpServer.Shutdown(ctx)
}
