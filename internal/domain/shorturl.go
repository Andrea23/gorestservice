package domain

import "time"

type ShortUrl struct {
	ID          int
	OwnerID     int
	CreateDate  time.Time
	OriginalURL string
	NewURL      string
	Visits      int
}

type Referer struct {
	ID         int
	ShortUrlID int
	Source     string
}
