package domain

type User struct {
	ID       int
	Name     string
	Password string
	Email    string
}
