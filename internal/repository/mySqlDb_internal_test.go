package repository

import (
	"reflect"
	"testing"
	"time"

	"bitbucket.org/Andrea23/gorestservice/internal/config"
	"bitbucket.org/Andrea23/gorestservice/internal/models"
)

func cmpUsers(user *models.User, newuser *models.User) bool {
	return user.Name == newuser.Name && user.Password == newuser.Password && user.Email == newuser.Email
}

func cmpShortUrls(shorturl *models.Shorturl, newshorturl *models.Shorturl) bool {
	return shorturl.OwnerID == newshorturl.OwnerID && shorturl.OriginalURL == newshorturl.OriginalURL && shorturl.NewURL == newshorturl.NewURL && shorturl.Visits == newshorturl.Visits
}

func cmpShortUrlsExt(shorturl *models.Shorturl, newshorturl *models.Shorturl) bool {
	return shorturl.OwnerID == newshorturl.OwnerID && shorturl.OriginalURL == newshorturl.OriginalURL && shorturl.NewURL == newshorturl.NewURL && shorturl.Visits == newshorturl.Visits && reflect.DeepEqual(shorturl.Referers, newshorturl.Referers)
}

func TestMakeInitMySQLRepo(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	if config.UseInMemoryRepository {
		t.Log("No testing, config.UseInMemoryRepository == true")
		return
	}

	baserepo, err := MakeInitMySQLRepo(config.MySQLConnectionString)
	if err != nil {
		t.Errorf("Cannot create repository: %s", err)
		return
	}

	defer baserepo.Close()

	err = resetTablesForTests()
	if err != nil {
		t.Errorf("Cannot reset repository: %s", err)
		return
	}

	// Users
	userrepo := GetMySQLUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err = userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	newuser, err := userrepo.GetU(user.Name)
	if err != nil {
		t.Errorf("Get user failed: %s", err)
		return
	}

	if !cmpUsers(&user, &newuser) {
		t.Error("users not the equal")
	}

	// ShortUrls
	user.ID = newuser.ID
	shorturlsrepo := GetMySQLShortUrlsRepo()

	shorturl := models.Shorturl{0, user.ID, time.Now(), "https://stulchik.com/yadivan.htm", "https://pupkin.ch/TEjOfb", 0, make([]string, 0)}
	err = shorturlsrepo.CreateS(shorturl)

	if err != nil {
		t.Errorf("cannot create url: %s", err)
	}

	newshorturl, err := shorturlsrepo.Find(user.ID, shorturl.NewURL)
	if err != nil {
		t.Errorf("cannot find url: %s", err)
	}

	if !cmpShortUrls(&shorturl, &newshorturl) {
		t.Error("shorturls not the equal")
	}

	shorturl.ID = newshorturl.ID
	urls, err := shorturlsrepo.AllUrls(user.ID, 0, 1)

	if err != nil {
		t.Errorf("cannot get urls: %s", err)
	}

	if len(urls) == 0 {
		t.Error("urls for user no exist")
	}

	shorturl.Visits++

	shorturl.Referers = append(shorturl.Referers, "localhost")
	err = shorturlsrepo.UpdateS(shorturl)

	if err != nil {
		t.Errorf("cannot update url: %s", err)
	}

	newshorturl, err = shorturlsrepo.Find(user.ID, shorturl.NewURL)
	if !cmpShortUrlsExt(&shorturl, &newshorturl) {
		t.Error("shorturls not the equal after update")
	}

	err = shorturlsrepo.DeleteS(shorturl.ID)
	if err != nil {
		t.Errorf("cannot delete url: %s", err)
	}

	_, err = shorturlsrepo.Find(user.ID, shorturl.NewURL)

	if err == nil {
		t.Error("url not deleted")
	}
}
