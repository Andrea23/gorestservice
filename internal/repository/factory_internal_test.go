package repository

import (
	"testing"

	"bitbucket.org/Andrea23/gorestservice/internal/config"
)

func TestCreateRepo(t *testing.T) {

	types := []RepositoryType{InMemDb, MySQLDb}

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	for _, _type := range types {

		if _type == MySQLDb && config.UseInMemoryRepository {
			continue
		}

		// Users
		_, _, err := CreateUsersRepo(config)

		if err != nil {
			t.Errorf("Create Users Repository failed: %s", err)
		}

		// ShortUrls
		_, _, err = CreateShortUrlsRepo(config)

		if err != nil {
			t.Errorf("Create ShortUrls Repository failed: %s", err)
		}
	}
}
