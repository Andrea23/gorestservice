package repository

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/Andrea23/gorestservice/internal/domain"
	"bitbucket.org/Andrea23/gorestservice/internal/interfaces"
	"bitbucket.org/Andrea23/gorestservice/internal/models"
	"github.com/go-sql-driver/mysql"
)

type mySqlDB struct {
	inst *sql.DB
}

var instMySQL *mySqlDB

func MakeInitMySQLRepo(dataSourceName string) (interfaces.BaseRepository, error) {

	if instMySQL == nil {

		db, err := sql.Open("mysql", dataSourceName)
		if err != nil {
			return nil, err
		}

		if err = db.Ping(); err != nil {
			return nil, err
		}

		instMySQL = &mySqlDB{db}
	}

	return instMySQL, nil
}

func (db *mySqlDB) Close() error {
	if instMySQL == nil {
		return instMySQL.inst.Close()
	}

	return nil
}

func resetTablesForTests() error {

	tableFileQueries := []string{
		"../../sqlQueries/create_table_users.sql",
		"../../sqlQueries/create_table_shorturls.sql",
		"../../sqlQueries/create_table_referers.sql"}

	tableQueries := make([]string, 0)

	// Read .sql file queries
	for _, val := range tableFileQueries {

		sqlFile, err := os.Open(val)
		if err != nil {
			return err
		}

		defer sqlFile.Close()
		bytes, err := ioutil.ReadAll(sqlFile)
		if err != nil {
			return err
		}

		s := string(bytes[:])
		tableQueries = append(tableQueries, s)
	}

	// exec SQL queries
	for _, q := range tableQueries {

		_, err := instMySQL.inst.Exec(q)
		if err != nil {

			mysqlErr := err.(*mysql.MySQLError)
			if mysqlErr != nil && mysqlErr.Number == 1050 {
				continue
			}

			return err
		}
	}

	// reset Tables
	tables := []string{"referers", "shorturls", "users"}
	query := "DELETE FROM %s"

	for _, val := range tables {

		q := fmt.Sprintf(query, val)
		_, err := instMySQL.inst.Exec(q)

		if err != nil {
			return err
		}
	}

	return nil
}

// GetMySQLUsersRepo create or get Users db based repository
func GetMySQLUsersRepo() interfaces.UserRepository {

	if instMySQL == nil {
		log.Panic("Repository not created\nCall MakeInitRepo()")
	}

	return instMySQL
}

// GetMySQLShortUrlsRepo create or get ShortUrls db based repository
func GetMySQLShortUrlsRepo() interfaces.ShortUrlRepository {

	if instMySQL == nil {
		log.Panic("Repository not created\nCall MakeInitRepo()")
	}

	return instMySQL
}

func copyUserFromDbToModel(dbUser *domain.User, modelUser *models.User) {

	modelUser.ID = dbUser.ID
	modelUser.Name = dbUser.Name
	modelUser.Password = dbUser.Password
	modelUser.Email = dbUser.Email
}

func copyUserFromModelToDb(modelUser *models.User, dbUser *domain.User) {

	dbUser.ID = modelUser.ID
	dbUser.Name = modelUser.Name
	dbUser.Password = modelUser.Password
	dbUser.Email = modelUser.Email
}

func copyShortUrlFromDbToModel(dbShortUrl *domain.ShortUrl, modelShortUrl *models.Shorturl) {

	modelShortUrl.ID = dbShortUrl.ID
	modelShortUrl.OwnerID = dbShortUrl.OwnerID
	modelShortUrl.TimeStamp = dbShortUrl.CreateDate
	modelShortUrl.OriginalURL = dbShortUrl.OriginalURL
	modelShortUrl.NewURL = dbShortUrl.NewURL
	modelShortUrl.Visits = dbShortUrl.Visits
}

func copyShortUrlFromModelToDb(modelShortUrl *models.Shorturl, dbShortUrl *domain.ShortUrl) {

	dbShortUrl.ID = modelShortUrl.ID
	dbShortUrl.OwnerID = modelShortUrl.OwnerID
	dbShortUrl.CreateDate = modelShortUrl.TimeStamp
	dbShortUrl.OriginalURL = modelShortUrl.OriginalURL
	dbShortUrl.NewURL = modelShortUrl.NewURL
	dbShortUrl.Visits = modelShortUrl.Visits
}

func transact(db *mySqlDB, txFunc func(*sql.Tx) error) (err error) {

	tx, err := db.inst.Begin()
	if err != nil {
		return
	}

	defer func() {
		if p := recover(); p != nil {
			err = tx.Rollback()
			panic(p) // re-throw panic after Rollback
		} else if err != nil {
			err = tx.Rollback()
		} else {
			err = tx.Commit()
		}
	}()

	err = txFunc(tx)
	return err
}

// Users
func (db *mySqlDB) GetU(userid string) (models.User, error) {

	query := "SELECT * FROM users WHERE name=?"
	row := db.inst.QueryRow(query, userid)

	dbUser := domain.User{}
	result := models.User{}

	err := row.Scan(&dbUser.ID, &dbUser.Name, &dbUser.Password, &dbUser.Email)
	if err != nil {
		return result, err
	}

	copyUserFromDbToModel(&dbUser, &result)
	return result, nil
}

func (db *mySqlDB) CreateU(user models.User) error {

	return transact(db, func(tx *sql.Tx) error {

		dbUser := domain.User{}
		copyUserFromModelToDb(&user, &dbUser)

		query := "INSERT INTO users (name, passwd, email) values (?, ?, ?)"

		_, err := tx.Exec(query, dbUser.Name, dbUser.Password, dbUser.Email)
		if err != nil {
			return err
		}

		return nil
	})
}

func (db *mySqlDB) DeleteU(userid string) error {

	return transact(db, func(tx *sql.Tx) error {

		query := "DELETE FROM users WHERE name=?"

		_, err := tx.Exec(query, userid)
		if err != nil {
			return err
		}

		return nil
	})
}

func (db *mySqlDB) UpdateU(user models.User) error {

	return transact(db, func(tx *sql.Tx) error {

		dbUser := domain.User{}
		copyUserFromModelToDb(&user, &dbUser)

		query := "UPDATE users SET name=?, passwd=?, email=? WHERE id=?"

		_, err := tx.Exec(query, dbUser.Name, dbUser.Password, dbUser.Email, dbUser.ID)
		if err != nil {
			return err
		}

		return nil
	})
}

// ShortUrl
func (db *mySqlDB) CreateS(shortURL models.Shorturl) error {

	return transact(db, func(tx *sql.Tx) error {

		dbShortUrl := domain.ShortUrl{}
		copyShortUrlFromModelToDb(&shortURL, &dbShortUrl)

		query := "INSERT INTO shorturls (ownerid, createdate, originalurl, newurl, visits) VALUES (?, ?, ?, ?, ?)"

		_, err := tx.Exec(query, dbShortUrl.OwnerID, dbShortUrl.CreateDate, dbShortUrl.OriginalURL, dbShortUrl.NewURL, dbShortUrl.Visits)
		if err != nil {
			return err
		}

		return nil
	})
}

func (db *mySqlDB) Find(userid int, shortURL string) (models.Shorturl, error) {

	query := "SELECT * FROM shorturls WHERE ownerid=? AND newurl=?"
	row := db.inst.QueryRow(query, userid, shortURL)

	dbShortUrl := domain.ShortUrl{}
	result := models.Shorturl{}

	err := row.Scan(&dbShortUrl.ID, &dbShortUrl.OwnerID, &dbShortUrl.CreateDate, &dbShortUrl.OriginalURL, &dbShortUrl.NewURL, &dbShortUrl.Visits)
	if err != nil {
		return result, err
	}

	copyShortUrlFromDbToModel(&dbShortUrl, &result)

	result.Referers = make([]string, 0)
	query = "SELECT * FROM referers WHERE shorturlid=?"

	rows, err := db.inst.Query(query, dbShortUrl.ID)
	if err != nil {
		return result, err
	}
	defer rows.Close()

	for rows.Next() {
		val := domain.Referer{}
		err = rows.Scan(&val.ID, &val.ShortUrlID, &val.Source)

		if err != nil {
			return result, err
		}

		result.Referers = append(result.Referers, val.Source)
	}

	return result, nil
}

func (db *mySqlDB) AllUrls(userid int, start uint16, limit uint16) ([]*models.Shorturl, error) {

	result := make([]*models.Shorturl, 0)
	query := "SELECT * FROM shorturls WHERE ownerid=? LIMIT %d, %d"
	query = fmt.Sprintf(query, start, limit)

	rows, err := db.inst.Query(query, userid)
	if err != nil {
		return result, err
	}
	defer rows.Close()

	for rows.Next() {
		dbShortUrl := domain.ShortUrl{}
		modShortUrl := models.Shorturl{}

		err = rows.Scan(&dbShortUrl.ID, &dbShortUrl.OwnerID, &dbShortUrl.CreateDate, &dbShortUrl.OriginalURL, &dbShortUrl.NewURL, &dbShortUrl.Visits)

		if err != nil {
			return result, err
		}

		copyShortUrlFromDbToModel(&dbShortUrl, &modShortUrl)
		result = append(result, &modShortUrl)
	}

	for _, shorturl := range result {

		shorturl.Referers = make([]string, 0)
		query = "SELECT * FROM referers WHERE shorturlid=?"

		rows, err := db.inst.Query(query, shorturl.ID)
		if err != nil {
			return result, err
		}
		defer rows.Close()

		for rows.Next() {
			val := domain.Referer{}
			err = rows.Scan(&val.ID, &val.ShortUrlID, &val.Source)

			if err != nil {
				return result, err
			}

			shorturl.Referers = append(shorturl.Referers, val.Source)
		}
	}

	return result, nil
}

func (db *mySqlDB) DeleteS(shortUrlID int) error {

	return transact(db, func(tx *sql.Tx) error {

		queries := [2]string{"DELETE FROM referers WHERE shorturlid=?", "DELETE FROM shorturls WHERE id=?"}

		for _, q := range queries {
			_, err := tx.Exec(q, shortUrlID)

			if err != nil {
				return err
			}
		}

		return nil
	})
}

func (db *mySqlDB) UpdateS(shortURL models.Shorturl) error {

	return transact(db, func(tx *sql.Tx) error {

		dbShortUrl := domain.ShortUrl{}
		copyShortUrlFromModelToDb(&shortURL, &dbShortUrl)

		query := "UPDATE shorturls SET ownerid=?, createdate=?, originalurl=?, newurl=?, visits=? WHERE id=?"
		_, err := db.inst.Exec(query, dbShortUrl.OwnerID, dbShortUrl.CreateDate, dbShortUrl.OriginalURL, dbShortUrl.NewURL, dbShortUrl.Visits, dbShortUrl.ID)

		if err != nil {
			return err
		}

		query = "INSERT INTO referers (shorturlid, source) VALUES (?, ?)"
		for _, val := range shortURL.Referers {

			ref := domain.Referer{0, shortURL.ID, val}
			_, err := db.inst.Exec(query, ref.ShortUrlID, ref.Source)

			if err != nil {
				return err
			}
		}

		return nil
	})
}
