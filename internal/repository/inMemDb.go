package repository

import (
	"sync"

	"bitbucket.org/Andrea23/gorestservice/internal/interfaces"
	"bitbucket.org/Andrea23/gorestservice/internal/models"
)

type inMemoryDB struct {
	users []*models.User
	urls  []*models.Shorturl
	lck   sync.RWMutex
}

var inst *inMemoryDB

func makeInMemRepo() {

	if inst == nil {
		inst = &inMemoryDB{users: make([]*models.User, 0), urls: make([]*models.Shorturl, 0)}
	}

	inst.lck.Lock()
	defer inst.lck.Unlock()

	if len(inst.users) > 0 {
		inst.users = make([]*models.User, 0)
	}

	if len(inst.urls) > 0 {
		inst.urls = make([]*models.Shorturl, 0)
	}
}

// GetInMemoryUsersRepo create or get Users memory based repository
func GetInMemoryUsersRepo() interfaces.UserRepository {

	makeInMemRepo()
	return inst
}

// GetInMemoryShortUrlsRepo create or get ShortUrls memory based repository
func GetInMemoryShortUrlsRepo() interfaces.ShortUrlRepository {

	makeInMemRepo()
	return inst
}

func (d *inMemoryDB) GetU(userid string) (models.User, error) {

	d.lck.RLock()
	defer d.lck.RUnlock()

	var index = -1
	for idx, value := range d.users {
		if value.Name == userid {
			index = idx
			break
		}
	}

	result := models.User{}
	if index < 0 {
		return result, models.ErrNotFound
	}

	result = *d.users[index]
	return result, nil
}

var currentUserID = 1

func (d *inMemoryDB) CreateU(user models.User) error {

	d.lck.Lock()
	defer d.lck.Unlock()

	if user.ID == 0 {
		user.ID = currentUserID
		currentUserID++
	}

	d.users = append(d.users, &user)
	return nil
}

func (d *inMemoryDB) DeleteU(userid string) error {

	d.lck.RLock()
	defer d.lck.RUnlock()

	var index = -1
	for idx, value := range d.users {
		if value.Name == userid {
			index = idx
			break
		}
	}

	if index < 0 {
		return models.ErrNotFound
	}

	a := d.users[:]
	a[index] = a[len(a)-1]
	a[len(a)-1] = nil
	a = a[:len(a)-1]
	d.users = a

	return nil
}

func (d *inMemoryDB) UpdateU(user models.User) error {

	d.lck.RLock()
	defer d.lck.RUnlock()

	var index = -1
	for idx, value := range d.users {
		if value.Name == user.Name {
			index = idx
			break
		}
	}

	if index < 0 {
		return models.ErrNotFound
	}

	d.users[index] = &user

	return nil
}

// ShortUrl
func (d *inMemoryDB) CreateS(shortURL models.Shorturl) error {

	d.lck.Lock()
	defer d.lck.Unlock()

	d.urls = append(d.urls, &shortURL)
	return nil
}

func (d *inMemoryDB) Find(userid int, shortURL string) (models.Shorturl, error) {

	d.lck.RLock()
	defer d.lck.RUnlock()

	for _, val := range d.urls {
		if val.OwnerID == userid && val.NewURL == shortURL {
			return *val, nil
		}
	}

	return models.Shorturl{}, models.ErrNotFound
}

func (d *inMemoryDB) AllUrls(userid int, start uint16, limit uint16) ([]*models.Shorturl, error) {

	d.lck.RLock()
	defer d.lck.RUnlock()

	result := make([]*models.Shorturl, 0)
	for _, val := range d.urls {
		if val.OwnerID == userid {
			result = append(result, val)
		}
	}

	length := (uint16)(len(result))
	if length == 0 {
		return nil, models.ErrNotFound
	}

	if length < start {
		return nil, models.ErrOutOfRange
	}

	var count uint16

	if length-start < limit {
		count = length - start
	} else {
		count = limit
	}

	result = result[start : count+start]
	return result, nil
}

func (d *inMemoryDB) DeleteS(shortUrlID int) error {

	d.lck.Lock()
	defer d.lck.Unlock()

	var idx = -1
	for i, val := range d.urls {
		if val.ID == shortUrlID {
			idx = i
			break
		}
	}

	if idx < 0 {
		return models.ErrNotFound
	}

	a := d.urls[:]
	a[idx] = a[len(a)-1]
	a[len(a)-1] = nil
	a = a[:len(a)-1]
	d.urls = a

	return nil
}

func (d *inMemoryDB) UpdateS(shortURL models.Shorturl) error {

	d.lck.Lock()
	defer d.lck.Unlock()

	var idx = -1
	for i, val := range d.urls {
		if val.ID == shortURL.ID {
			idx = i
			break
		}
	}

	if idx < 0 {
		return models.ErrNotFound
	}
	d.urls[idx] = &shortURL

	return nil
}
