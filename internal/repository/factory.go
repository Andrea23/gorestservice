package repository

import (
	"errors"
	"fmt"

	"bitbucket.org/Andrea23/gorestservice/internal/config"
	"bitbucket.org/Andrea23/gorestservice/internal/interfaces"
)

// CreateUsersRepo factory method for "User Repository"
func CreateUsersRepo(config config.Values) (interfaces.BaseRepository, interfaces.UserRepository, error) {

	typeofrepo := InMemDb
	if !config.UseInMemoryRepository {
		typeofrepo = MySQLDb
	}

	if typeofrepo == InMemDb {
		userrepo := GetInMemoryUsersRepo()
		return nil, userrepo, nil
	}

	if typeofrepo == MySQLDb {

		baserepo, err := MakeInitMySQLRepo(config.MySQLConnectionString)
		if err != nil {
			return nil, nil, err
		}

		userrepo := GetMySQLUsersRepo()
		return baserepo, userrepo, nil
	}

	return nil, nil, errors.New(fmt.Sprint("Unsupported support type"))
}

// CreateShortUrlsRepo factory method for "ShortUrl Repository"
func CreateShortUrlsRepo(config config.Values) (interfaces.BaseRepository, interfaces.ShortUrlRepository, error) {

	typeofrepo := InMemDb
	if !config.UseInMemoryRepository {
		typeofrepo = MySQLDb
	}

	if typeofrepo == InMemDb {
		shorturlsrepo := GetInMemoryShortUrlsRepo()
		return nil, shorturlsrepo, nil
	}

	if typeofrepo == MySQLDb {

		baserepo, err := MakeInitMySQLRepo(config.MySQLConnectionString)
		if err != nil {
			return nil, nil, err
		}

		shorturlsrepo := GetMySQLShortUrlsRepo()
		return baserepo, shorturlsrepo, nil
	}

	return nil, nil, errors.New(fmt.Sprint("Unsupported support type"))
}
