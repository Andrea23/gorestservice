package repository

// RepositoryType materialization
type RepositoryType int

const (
	// InMemDb for test only
	InMemDb RepositoryType = iota
	// MySQLDb for MySQL DB
	MySQLDb
)
