package repository

import (
	"strconv"
	"testing"
	"time"

	"bitbucket.org/Andrea23/gorestservice/internal/models"
)

func TestGetInMemoryUsersRepo(t *testing.T) {

	userrepo := GetInMemoryUsersRepo()

	if userrepo == nil {
		t.Error("Create InMemoryUsersRepo failed")
	}
}

func TestInMemoryUsersRepoCreateU(t *testing.T) {

	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}
}

func TestInMemoryUsersRepoGetUOK(t *testing.T) {

	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	newuser, err := userrepo.GetU(user.Name)
	if err != nil {
		t.Errorf("Get user failed: %s", err)
		return
	}

	if !cmpUsers(&user, &newuser) {
		t.Error("users not the equal")
	}
}

func TestInMemoryUsersRepoGetUFAILED(t *testing.T) {

	userrepo := GetInMemoryUsersRepo()

	_, err := userrepo.GetU("testUser")
	if err != models.ErrNotFound {
		t.Error("get unexist user")
	}
}

func TestInMemoryUsersRepoDeleteUFAILED(t *testing.T) {

	userrepo := GetInMemoryUsersRepo()

	err := userrepo.DeleteU("testUser")
	if err != models.ErrNotFound {
		t.Error("DeleteU unexist user")
	}
}

func TestInMemoryUsersRepoDeleteUOK(t *testing.T) {

	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	err = userrepo.DeleteU(user.Name)
	if err != nil {
		t.Errorf("DeleteU user failed: %s", err)
		return
	}

	_, err = userrepo.GetU(user.Name)
	if err != models.ErrNotFound {
		t.Errorf("DeleteU user failed: %s", err)
		return
	}
}

func TestInMemoryUsersRepoUpdateUFAILED(t *testing.T) {

	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}

	err := userrepo.UpdateU(user)
	if err != models.ErrNotFound {
		t.Error("UpdateU unexist user")
	}
}

func TestInMemoryUsersRepoUpdateUOK(t *testing.T) {

	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	user.Password = "Asdfgh"

	err = userrepo.UpdateU(user)
	if err != nil {
		t.Errorf("UpdateU user failed: %s", err)
		return
	}

	newuser, err := userrepo.GetU(user.Name)
	if err != nil {
		t.Errorf("Get user failed: %s", err)
		return
	}

	if !cmpUsers(&user, &newuser) {
		t.Error("users not the equal")
	}
}

func TestGetInMemoryShortUrlsRepo(t *testing.T) {

	shorturlsrepo := GetInMemoryShortUrlsRepo()

	if shorturlsrepo == nil {
		t.Error("Create InMemoryShortUrlsRepo failed")
	}
}

func TestGetInMemoryShortUrlsRepoCreateS(t *testing.T) {

	// Users
	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	newuser, err := userrepo.GetU(user.Name)
	if err != nil {
		t.Errorf("Get user failed: %s", err)
		return
	}

	// ShortUrls
	user.ID = newuser.ID
	shorturlsrepo := GetInMemoryShortUrlsRepo()

	shorturl := models.Shorturl{0, user.ID, time.Now(), "https://stulchik.com/yadivan.htm", "https://pupkin.ch/TEjOfb", 0, make([]string, 0)}
	err = shorturlsrepo.CreateS(shorturl)

	if err != nil {
		t.Errorf("cannot create url: %s", err)
	}

	urls, err := shorturlsrepo.AllUrls(user.ID, 0, 1)

	if len(urls) == 0 {
		t.Error("urls for user no exist")
	}
}

func TestGetInMemoryShortUrlsRepoFindOK(t *testing.T) {

	// Users
	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	newuser, err := userrepo.GetU(user.Name)
	if err != nil {
		t.Errorf("Get user failed: %s", err)
		return
	}

	// ShortUrls
	user.ID = newuser.ID
	shorturlsrepo := GetInMemoryShortUrlsRepo()

	shorturl := models.Shorturl{0, user.ID, time.Now(), "https://stulchik.com/yadivan.htm", "https://pupkin.ch/TEjOfb", 0, make([]string, 0)}
	err = shorturlsrepo.CreateS(shorturl)

	if err != nil {
		t.Errorf("cannot create url: %s", err)
	}

	newshorturl, err := shorturlsrepo.Find(user.ID, shorturl.NewURL)
	if err != nil {
		t.Errorf("cannot find url: %s", err)
	}

	if !cmpShortUrls(&shorturl, &newshorturl) {
		t.Error("shorturls not the equal")
	}
}

func TestGetInMemoryShortUrlsRepoFindFAILED(t *testing.T) {

	shorturlsrepo := GetInMemoryShortUrlsRepo()

	_, err := shorturlsrepo.Find(0, "11111")
	if err != models.ErrNotFound {
		t.Error("Find unexist ShortUrl")
	}
}

func TestGetInMemoryShortUrlsRepoDeleteSFAILED(t *testing.T) {

	shorturlsrepo := GetInMemoryShortUrlsRepo()

	err := shorturlsrepo.DeleteS(0)
	if err != models.ErrNotFound {
		t.Error("DeleteS unexist ShortUrl")
	}
}

func TestGetInMemoryShortUrlsRepoUpdateSFAILED(t *testing.T) {

	shorturlsrepo := GetInMemoryShortUrlsRepo()

	shorturl := models.Shorturl{0, 0, time.Now(), "https://stulchik.com/yadivan.htm", "https://pupkin.ch/TEjOfb", 0, make([]string, 0)}

	err := shorturlsrepo.UpdateS(shorturl)
	if err != models.ErrNotFound {
		t.Error("UpdateS unexist ShortUrl")
	}
}

func TestGetInMemoryShortUrlsRepoUpdateSOK(t *testing.T) {

	// Users
	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	newuser, err := userrepo.GetU(user.Name)
	if err != nil {
		t.Errorf("Get user failed: %s", err)
		return
	}

	// ShortUrls
	user.ID = newuser.ID
	shorturlsrepo := GetInMemoryShortUrlsRepo()

	shorturl := models.Shorturl{0, user.ID, time.Now(), "https://stulchik.com/yadivan.htm", "https://pupkin.ch/TEjOfb", 0, make([]string, 0)}
	err = shorturlsrepo.CreateS(shorturl)

	if err != nil {
		t.Errorf("cannot create url: %s", err)
	}

	shorturl.Visits++

	shorturl.Referers = append(shorturl.Referers, "localhost")
	err = shorturlsrepo.UpdateS(shorturl)

	if err != nil {
		t.Errorf("cannot update url: %s", err)
	}

	newshorturl, err := shorturlsrepo.Find(user.ID, shorturl.NewURL)
	if !cmpShortUrlsExt(&shorturl, &newshorturl) {
		t.Error("shorturls not the equal after update")
	}
}

func TestGetInMemoryShortUrlsRepoDeleteSOK(t *testing.T) {

	// Users
	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	newuser, err := userrepo.GetU(user.Name)
	if err != nil {
		t.Errorf("Get user failed: %s", err)
		return
	}

	if !cmpUsers(&user, &newuser) {
		t.Error("users not the equal")
	}

	// ShortUrls
	user.ID = newuser.ID
	shorturlsrepo := GetInMemoryShortUrlsRepo()

	shorturl := models.Shorturl{0, user.ID, time.Now(), "https://stulchik.com/yadivan.htm", "https://pupkin.ch/TEjOfb", 0, make([]string, 0)}
	err = shorturlsrepo.CreateS(shorturl)

	if err != nil {
		t.Errorf("cannot create url: %s", err)
	}

	err = shorturlsrepo.DeleteS(shorturl.ID)
	if err != nil {
		t.Errorf("cannot delete url: %s", err)
	}

	_, err = shorturlsrepo.Find(user.ID, shorturl.NewURL)

	if err == nil {
		t.Error("url not deleted")
	}
}

func TestGetInMemoryShortUrlsRepoAllUrlsNotFound(t *testing.T) {

	shorturlsrepo := GetInMemoryShortUrlsRepo()

	_, err := shorturlsrepo.AllUrls(0, 0, 1)
	if err != models.ErrNotFound {
		t.Error("AllUrls get unexist urls")
	}
}

func TestGetInMemoryShortUrlsRepoAllUrlsOutOfRange(t *testing.T) {

	// Users
	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	newuser, err := userrepo.GetU(user.Name)
	if err != nil {
		t.Errorf("Get user failed: %s", err)
		return
	}

	// ShortUrls
	user.ID = newuser.ID
	shorturlsrepo := GetInMemoryShortUrlsRepo()

	shorturl := models.Shorturl{0, user.ID, time.Now(), "https://stulchik.com/yadivan.htm", "https://pupkin.ch/TEjOfb", 0, make([]string, 0)}
	err = shorturlsrepo.CreateS(shorturl)

	if err != nil {
		t.Errorf("cannot create url: %s", err)
	}

	_, err = shorturlsrepo.AllUrls(user.ID, 10, 1)

	if err != models.ErrOutOfRange {
		t.Error("AllUrls false pass out of range")
	}
}

func TestGetInMemoryShortUrlsRepoAllUrls(t *testing.T) {

	cases := []struct {
		name  string
		start uint16
		limit uint16
		count uint16
		err   error
	}{
		{
			name:  "First right slice",
			start: 0,
			limit: 4,
			count: 4,
			err:   nil,
		},
		{
			name:  "Second right slice",
			start: 4,
			limit: 4,
			count: 4,
			err:   nil,
		},
		{
			name:  "3rd slice with cut off",
			start: 8,
			limit: 4,
			count: 2,
			err:   nil,
		},
		{
			name:  "Limit more than actual amount of result",
			start: 0,
			limit: 20,
			count: 10,
			err:   nil,
		},
	}

	// Users
	userrepo := GetInMemoryUsersRepo()
	user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	err := userrepo.CreateU(user)

	if err != nil {
		t.Errorf("Create user failed: %s", err)
		return
	}

	newuser, err := userrepo.GetU(user.Name)
	if err != nil {
		t.Errorf("Get user failed: %s", err)
		return
	}

	// ShortUrls
	user.ID = newuser.ID
	shorturlsrepo := GetInMemoryShortUrlsRepo()

	for i := 0; i < 10; i++ {

		n := strconv.Itoa(i)
		shorturl := models.Shorturl{0, user.ID, time.Now(), "https://stulchik.com/" + n + "yadivan.htm", "https://pupkin.ch/TEjOfb" + n, 0, make([]string, 0)}
		_ = shorturlsrepo.CreateS(shorturl)
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {

			urls, err := shorturlsrepo.AllUrls(user.ID, tc.start, tc.limit)
			if err != nil {
				t.Fatalf("Get AllUrls failed: %s", err)
			}

			if len(urls) != int(tc.count) {
				t.Errorf("Expected: %v, got: %v", tc.count, len(urls))
			}
		})
	}
}
