package interfaces

import "bitbucket.org/Andrea23/gorestservice/internal/models"

type ShortUrlService interface {
	CreateS(shortURL models.Shorturl) error
	Find(userid int, shortURL string) (models.Shorturl, error)
	AllUrls(userid int) ([]*models.Shorturl, error)
	Delete(shortUrlID int) error
	Update(shortURL models.Shorturl) error
}
