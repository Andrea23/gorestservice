package interfaces

type BaseRepository interface {
	Close() error
}
