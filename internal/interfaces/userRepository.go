package interfaces

import "bitbucket.org/Andrea23/gorestservice/internal/models"

type UserRepository interface {
	CreateU(user models.User) error
	GetU(userid string) (models.User, error)
	DeleteU(userid string) error
	UpdateU(user models.User) error
}
