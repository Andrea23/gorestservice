package interfaces

import "bitbucket.org/Andrea23/gorestservice/internal/models"

type UserService interface {
	CreateU(user models.User) error
	User(userid string) (models.User, error)
}
