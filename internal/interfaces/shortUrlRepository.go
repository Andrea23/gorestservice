package interfaces

import "bitbucket.org/Andrea23/gorestservice/internal/models"

type ShortUrlRepository interface {
	CreateS(shortURL models.Shorturl) error
	Find(userid int, shortURL string) (models.Shorturl, error)
	AllUrls(userid int, start uint16, limit uint16) ([]*models.Shorturl, error)
	DeleteS(shortUrlID int) error
	UpdateS(shortURL models.Shorturl) error
}
