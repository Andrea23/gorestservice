package utils

import (
	"fmt"
	"net/http"

	"bitbucket.org/Andrea23/gorestservice/internal/models"
)

func CheckForServiceFetchError(w http.ResponseWriter, err error) bool {

	if err == models.ErrNotFound {
		http.Error(w, "not found", http.StatusNotFound)
		return true
	} else if err != nil {
		http.Error(w, fmt.Sprintf("error getting value from service: %s", err), http.StatusInternalServerError)
		return true
	}

	return false
}

func CheckForReqPostParams(w http.ResponseWriter, r *http.Request, params []string) bool {

	err := r.ParseForm()
	if err != nil {
		http.Error(w, fmt.Sprintf("missing params in query string: %s", err), http.StatusBadRequest)
		return true
	}

	for _, val := range params {
		res := r.Form.Get(val)

		if len(res) == 0 {
			msg := fmt.Sprintf("missing '%s' in query string", val)
			http.Error(w, msg, http.StatusBadRequest)

			return true
		}
	}

	return false
}
