package utils

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"bitbucket.org/Andrea23/gorestservice/internal/models"
)

func TestCheckForServiceFetchErrorOK(t *testing.T) {

	rr := httptest.NewRecorder()
	c := CheckForServiceFetchError(rr, nil)

	if c {
		t.Error("got error")
	}
}

func TestCheckForServiceFetchErrorNotFound(t *testing.T) {

	rr := httptest.NewRecorder()
	err := models.ErrNotFound
	c := CheckForServiceFetchError(rr, err)

	if !c {
		t.Error("wrong check")
	}
}

func TestCheckForServiceFetchAnotherError(t *testing.T) {

	rr := httptest.NewRecorder()
	err := errors.New("Another Error")

	c := CheckForServiceFetchError(rr, err)

	if !c {
		t.Error("wrong check")
	}
}

func TestCheckForReqPostParamsOK(t *testing.T) {

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	form := url.Values{}
	form.Add("userid", userid)
	form.Add("email", email)
	form.Add("passwd", passwd)

	req, err := http.NewRequest(http.MethodPost, "/users/registration", strings.NewReader(form.Encode()))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	rr := httptest.NewRecorder()
	params := []string{"userid", "email", "passwd"}

	c := CheckForReqPostParams(rr, req, params)

	if c {
		t.Error("something wrong")
	}
}

func TestCheckForReqPostParamsFAIL(t *testing.T) {

	userid := "Vasya"
	email := "pupkin@dot.com"

	form := url.Values{}
	form.Add("userid", userid)
	form.Add("email", email)

	req, err := http.NewRequest(http.MethodPost, "/users/registration", strings.NewReader(form.Encode()))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	rr := httptest.NewRecorder()
	params := []string{"userid", "email", "passwd"}

	c := CheckForReqPostParams(rr, req, params)

	if !c {
		t.Error("something wrong")
	}
}
