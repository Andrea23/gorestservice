package utils

import (
	"testing"
)

func TestGenerateNewUrl(t *testing.T) {

	newURL := GenerateNewUrl()
	baseURL := "https://pupkin.ch/"
	baselen := len(baseURL) + 6

	if baselen != len(newURL) {
		t.Error("Incorrect length of url")
	}
}
