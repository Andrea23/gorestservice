package utils

import (
	"testing"
)

func TestHashPassword(t *testing.T) {

	passwdplain := "Secret123"
	passwdhash, err := HashPassword(passwdplain)

	if len(passwdhash) <= 0 {
		t.Error("Incorrect length of password hash")
	}

	if err != nil {
		t.Error("password hashing error: " + err.Error())
	}

	if passwdplain == passwdhash {
		t.Error("password not hashed")
	}
}

func TestCheckPasswordHash(t *testing.T) {

	passwdplain := "Secret123"
	passwdhash, err := HashPassword(passwdplain)

	if err != nil {
		t.Error("password hashing error: " + err.Error())
	}

	c := CheckPasswordHash(passwdplain, passwdhash)

	if !c {
		t.Error("wrong passwd hash")
	}
}
