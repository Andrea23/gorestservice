package services

import (
	"reflect"
	"testing"

	"bitbucket.org/Andrea23/gorestservice/internal/config"
	"bitbucket.org/Andrea23/gorestservice/internal/repository"
)

func TestShortUrlService(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	_, userrepo, err := repository.CreateUsersRepo(config)
	if err != nil {
		t.Errorf("Cannot create Users repository: %s", err)
	}

	_, shorturlrepo, err := repository.CreateShortUrlsRepo(config)
	if err != nil {
		t.Errorf("Cannot create ShortUrls repository: %s", err)
	}

	shorturlsrv := MakeShortUrlService(shorturlrepo)
	usersrv := MakeUserService(userrepo)

	//user := models.User{0, "Vasya", "Qwerty", "pupkin@dot.com"}
	//shorturl := models.Shorturl{0, 0, time.Now(), "https://stulchik.com/yadivan.htm", "https://pupkin.ch/TEjOfb", 0, make([]string, 0)}

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	errService := usersrv.Registration(userid, email, passwd)
	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	user, errService := usersrv.User(userid)
	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	origurl := "https://stulchik.com/yadivan.htm"
	shorturl, errService := shorturlsrv.CreateS(user.ID, origurl)
	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	existshorturl, errService := shorturlsrv.Find(user.ID, shorturl.NewURL)
	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	if !reflect.DeepEqual(shorturl, existshorturl) {
		t.Error("shorturls not the equal")
	}

	urls, errService := shorturlsrv.AllUrls(user.ID, 0, 1)
	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	if len(urls) == 0 {
		t.Error("urls for user no exist")
	}

	shorturl.Visits++

	shorturl.Referers = append(shorturl.Referers, "localhost")
	errService = shorturlsrv.Update(shorturl)

	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	existshorturl, errService = shorturlsrv.Find(user.ID, shorturl.NewURL)
	if !reflect.DeepEqual(shorturl, existshorturl) {
		t.Error("shorturls not the equal after update")
	}

	errService = shorturlsrv.Delete(shorturl.ID)
	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	existshorturl, errService = shorturlsrv.Find(user.ID, shorturl.NewURL)
	if len(existshorturl.OriginalURL) > 0 {
		t.Error("shorturl doesnot deleted")
	}
}
