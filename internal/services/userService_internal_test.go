package services

import (
	"testing"

	"bitbucket.org/Andrea23/gorestservice/internal/config"
	"bitbucket.org/Andrea23/gorestservice/internal/models"
	"bitbucket.org/Andrea23/gorestservice/internal/repository"
	"bitbucket.org/Andrea23/gorestservice/internal/utils"
)

func TestCheckBasicAuthOK(t *testing.T) {

	inst := MakeUserService(nil)

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	passwdHash, err := utils.HashPassword(passwd)
	if err != nil {
		t.Errorf("Cannot create password hash: %s", err)
	}

	user := models.User{0, userid, passwdHash, email}

	errService := inst.CheckBasicAuth(user, userid, passwd)
	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}
}

func TestCheckBasicAuthFAILByName(t *testing.T) {

	inst := MakeUserService(nil)

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	passwdHash, err := utils.HashPassword(passwd)
	if err != nil {
		t.Errorf("Cannot create password hash: %s", err)
	}

	user := models.User{0, userid + "1", passwdHash, email}

	errService := inst.CheckBasicAuth(user, userid, passwd)
	if errService.Status != models.StatusServiceUnauthorized {
		t.Error("False passed")
	}
}

func TestCheckBasicAuthFAILByPasswd(t *testing.T) {

	inst := MakeUserService(nil)

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	user := models.User{0, userid, "4234ewqrerw", email}

	errService := inst.CheckBasicAuth(user, userid, passwd)
	if errService.Status != models.StatusServiceUnauthorized {
		t.Error("False passed")
	}
}

func TestUserRegistrationOK(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	_, userrepo, err := repository.CreateUsersRepo(config)
	if err != nil {
		t.Errorf("Cannot create Users repository: %s", err)
	}

	inst := MakeUserService(userrepo)

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	passwdHash, err := utils.HashPassword(passwd)
	if err != nil {
		t.Errorf("Cannot create password hash: %s", err)
	}

	errService := inst.Registration(userid, email, passwdHash)

	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}
}

func TestUserRegistrationFAILED(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	_, userrepo, err := repository.CreateUsersRepo(config)
	if err != nil {
		t.Errorf("Cannot create Users repository: %s", err)
	}

	inst := MakeUserService(userrepo)

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	passwdHash, err := utils.HashPassword(passwd)
	if err != nil {
		t.Errorf("Cannot create password hash: %s", err)
	}

	errService := inst.Registration(userid, email, passwdHash)

	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	errService = inst.Registration(userid, email, passwdHash)

	if errService.Status != models.StatusInternalServiceError {
		t.Error(errService.Err.Error())
	}
}

func TestGetUserOK(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	_, userrepo, err := repository.CreateUsersRepo(config)
	if err != nil {
		t.Errorf("Cannot create Users repository: %s", err)
	}

	inst := MakeUserService(userrepo)

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	passwdHash, err := utils.HashPassword(passwd)
	if err != nil {
		t.Errorf("Cannot create password hash: %s", err)
	}

	errService := inst.Registration(userid, email, passwdHash)

	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	user, errService := inst.User(userid)
	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	if user.Name != userid || user.Email != email || user.Password != passwdHash {
		t.Error("users not the equal")
	}
}

func TestGetUserFAILED(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	_, userrepo, err := repository.CreateUsersRepo(config)
	if err != nil {
		t.Errorf("Cannot create Users repository: %s", err)
	}

	inst := MakeUserService(userrepo)

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	passwdHash, err := utils.HashPassword(passwd)
	if err != nil {
		t.Errorf("Cannot create password hash: %s", err)
	}

	errService := inst.Registration(userid, email, passwdHash)

	if errService.Err != nil {
		t.Error(errService.Err.Error())
	}

	_, errService = inst.User(userid + "1")
	if errService.Status != models.StatusInternalServiceError {
		t.Error(errService.Err.Error())
	}
}
