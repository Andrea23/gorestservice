package services

import (
	"errors"
	"fmt"
	"log"

	"bitbucket.org/Andrea23/gorestservice/internal/interfaces"
	"bitbucket.org/Andrea23/gorestservice/internal/models"
	"bitbucket.org/Andrea23/gorestservice/internal/utils"
)

type UserService struct {
	repo interfaces.UserRepository
}

func MakeUserService(repo interfaces.UserRepository) UserService {

	result := UserService{repo}
	return result
}

func (service *UserService) Registration(userid string, email string, passwd string) models.ServiceError {

	checkUserRepo(service)
	existUser, err := service.repo.GetU(userid)
	if len(existUser.Name) > 0 {
		return models.ServiceError{Err: fmt.Errorf("cannot create user: %s", "name exist"), Status: models.StatusInternalServiceError}
	}

	user := models.User{0, userid, passwd, email}
	err = service.repo.CreateU(user)

	if err != nil {
		return models.ServiceError{Err: fmt.Errorf("cannot create user: %s", err), Status: models.StatusInternalServiceError}
	}

	return models.ServiceError{Err: nil, Status: models.StatusOK}
}

func (service *UserService) User(userid string) (models.User, models.ServiceError) {

	checkUserRepo(service)
	existUser, err := service.repo.GetU(userid)
	if err != nil {
		return models.User{}, models.ServiceError{Err: errors.New("invalid user"), Status: models.StatusInternalServiceError}
	}

	return existUser, models.ServiceError{Err: nil, Status: models.StatusOK}
}

func (service *UserService) CheckBasicAuth(user models.User, userid string, passwd string) models.ServiceError {

	if userid != user.Name || !utils.CheckPasswordHash(passwd, user.Password) {
		return models.ServiceError{Err: errors.New("not authorized"), Status: models.StatusServiceUnauthorized}
	}

	return models.ServiceError{Err: nil, Status: models.StatusOK}
}

func (service *UserService) PrintUser(user models.User) (string, models.ServiceError) {

	// if user == nil {
	// 	return "", models.ServiceError{fmt.Errorf("operation failed: %s", err), models.StatusInternalServiceError}
	// }

	result := printUserPlain(user)
	return result, models.ServiceError{Err: nil, Status: models.StatusOK}
}

func printUserPlain(user models.User) string {

	result := fmt.Sprintf("User ID: %d\nE-Mail: %s\n", user.ID, user.Email)
	return result
}

func checkUserRepo(service *UserService) {

	if service.repo == nil {
		log.Panic("No 'users repo'")
	}
}
