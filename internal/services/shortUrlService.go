package services

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/Andrea23/gorestservice/internal/interfaces"
	"bitbucket.org/Andrea23/gorestservice/internal/models"
	"bitbucket.org/Andrea23/gorestservice/internal/utils"
)

type ShortUrlService struct {
	repo interfaces.ShortUrlRepository
}

func MakeShortUrlService(repo interfaces.ShortUrlRepository) ShortUrlService {

	result := ShortUrlService{repo}
	return result
}

func (service *ShortUrlService) CreateS(userid int, origurl string) (models.Shorturl, models.ServiceError) {

	newurl := utils.GenerateNewUrl()
	shorturl := models.Shorturl{0, userid, time.Now(), origurl, newurl, 0, make([]string, 0)}

	checkShortUrlRepo(service)
	err := service.repo.CreateS(shorturl)

	if err != nil {
		return models.Shorturl{}, models.ServiceError{fmt.Errorf("cannot create short url: %s", err), models.StatusInternalServiceError}
	}

	return shorturl, models.ServiceError{Status: models.StatusOK}
}

func (service *ShortUrlService) Find(userid int, shortURL string) (models.Shorturl, models.ServiceError) {

	checkShortUrlRepo(service)
	existshorturl, err := service.repo.Find(userid, shortURL)
	if err != nil {
		return models.Shorturl{}, models.ServiceError{fmt.Errorf("cannot find short url: %s", err), models.StatusInternalServiceError}
	}

	return existshorturl, models.ServiceError{Status: models.StatusOK}
}

func (service *ShortUrlService) AllUrls(userid int, start uint16, limit uint16) ([]*models.Shorturl, models.ServiceError) {

	checkShortUrlRepo(service)
	urls, err := service.repo.AllUrls(userid, start, limit)
	if err != nil || len(urls) == 0 {
		return nil, models.ServiceError{fmt.Errorf("no short urls owned by user: %s", err), models.StatusInternalServiceError}
	}

	return urls, models.ServiceError{Status: models.StatusOK}
}

func (service *ShortUrlService) Delete(shortUrlID int) models.ServiceError {

	checkShortUrlRepo(service)
	err := service.repo.DeleteS(shortUrlID)
	if err != nil {
		return models.ServiceError{fmt.Errorf("cannot delete url: %s", err), models.StatusInternalServiceError}
	}

	return models.ServiceError{Status: models.StatusOK}
}

func (service *ShortUrlService) Update(shortURL models.Shorturl) models.ServiceError {

	checkShortUrlRepo(service)
	err := service.repo.UpdateS(shortURL)
	if err != nil {
		return models.ServiceError{fmt.Errorf("cannot update url: %s", err), models.StatusInternalServiceError}
	}

	return models.ServiceError{Status: models.StatusOK}
}

func checkShortUrlRepo(service *ShortUrlService) {

	if service.repo == nil {
		log.Panic("No 'ShortUrl repo'")
	}
}
