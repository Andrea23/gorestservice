package handlers

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"

	"bitbucket.org/Andrea23/gorestservice/internal/services"
	"bitbucket.org/Andrea23/gorestservice/internal/utils"
)

// ShorturlsCreate create new short url for user
func ShorturlsCreate(inst *services.ShortUrlService, userinst *services.UserService) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodPost {
			http.Error(w, http.StatusText(405), http.StatusBadRequest)
			return
		}

		params := []string{"src"}
		if utils.CheckForReqPostParams(w, r, params) {
			return
		}

		user, errService, authOK := checkHTTPAuth(userinst, w, r)
		if !checkHTTPAuthError(w, user, errService, authOK) {
			return
		}

		origurl := r.Form.Get("src")
		shorturl, errService := inst.CreateS(user.ID, origurl)
		if !checkHTTPError(w, errService) {
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "Your URL: %s\n", shorturl.NewURL)
	})
}

// ShorturlsOwned show urls of user
func ShorturlsOwned(inst *services.ShortUrlService, userinst *services.UserService) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodGet {
			http.Error(w, http.StatusText(405), http.StatusBadRequest)
			return
		}

		user, errService, authOK := checkHTTPAuth(userinst, w, r)
		if !checkHTTPAuthError(w, user, errService, authOK) {
			return
		}

		// limit=5&start=5

		startParam := r.URL.Query().Get("start")
		if startParam == "" {
			http.Error(w, "missing 'start' in query string", http.StatusBadRequest)
			return
		}

		start, err := strconv.ParseInt(startParam, 10, 16)
		if err != nil || start < 0 {
			http.Error(w, "invalid param 'start' in query string", http.StatusBadRequest)
			return
		}

		limitParam := r.URL.Query().Get("limit")
		if limitParam == "" {
			http.Error(w, "missing 'limit' in query string", http.StatusBadRequest)
			return
		}

		limit, err := strconv.ParseInt(limitParam, 10, 16)
		if err != nil || limit < 1 {
			http.Error(w, "invalid param 'limit' in query string", http.StatusBadRequest)
			return
		}

		urls, errService := inst.AllUrls(user.ID, (uint16)(start), (uint16)(limit))
		if !checkHTTPError(w, errService) {
			return
		}

		w.WriteHeader(http.StatusOK)
		//fmt.Fprint(w, urls)

		for _, value := range urls {
			fmt.Fprint(w, value.NewURL+"\n")
		}
	})
}

//ShorturlsAbout get info about concrete short url
func ShorturlsAbout(inst *services.ShortUrlService, userinst *services.UserService) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodGet {
			http.Error(w, http.StatusText(405), http.StatusBadRequest)
			return
		}

		user, errService, authOK := checkHTTPAuth(userinst, w, r)
		if !checkHTTPAuthError(w, user, errService, authOK) {
			return
		}

		shortURL := r.URL.Query().Get("src")
		if shortURL == "" {
			http.Error(w, "missing 'src' in query string", http.StatusBadRequest)
			return
		}

		value, errService := inst.Find(user.ID, shortURL)
		if utils.CheckForServiceFetchError(w, errService.Err) {
			return
		}

		w.WriteHeader(http.StatusOK)

		msg := fmt.Sprintf("ID: %d\nFull URL: %s\nCreation date: %s\nVisits: %d\n", value.ID, value.OriginalURL, value.TimeStamp.String(), value.Visits)
		fmt.Fprint(w, msg)
	})
}

// ShorturlsDelete delete url from service
func ShorturlsDelete(inst *services.ShortUrlService, userinst *services.UserService) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodDelete {
			http.Error(w, http.StatusText(405), http.StatusBadRequest)
			return
		}

		user, errService, authOK := checkHTTPAuth(userinst, w, r)
		if !checkHTTPAuthError(w, user, errService, authOK) {
			return
		}

		rx, _ := regexp.Compile(`\d+$`)
		shortURLId := rx.FindString(r.URL.Path)
		id, err := strconv.Atoi(shortURLId)

		if err != nil {
			http.Error(w, fmt.Sprintf("error deleting value, invalid id: %s", err), http.StatusInternalServerError)
			return
		}

		errService = inst.Delete(id)
		if errService.Err != nil {
			http.Error(w, fmt.Sprintf("error deleting value from database: %s", errService.Err), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusNoContent)
	})
}

// ShorturlsReverse get orignial url and redirect
func ShorturlsReverse(inst *services.ShortUrlService, userinst *services.UserService) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodPut {
			http.Error(w, http.StatusText(405), http.StatusBadRequest)
			return
		}

		user, errService, authOK := checkHTTPAuth(userinst, w, r)
		if !checkHTTPAuthError(w, user, errService, authOK) {
			return
		}

		body, _ := ioutil.ReadAll(r.Body)
		// TODO: check for valid 'url' format
		shortURL := string(body)
		if shortURL == "" {
			http.Error(w, "missing 'url' in query string", http.StatusBadRequest)
			return
		}

		value, errService := inst.Find(user.ID, shortURL)
		if utils.CheckForServiceFetchError(w, errService.Err) {
			return
		}

		value.Visits++
		ref := r.Referer()

		if ref == "" {
			ref = "empty"
		}

		value.Referers = append(value.Referers, ref)
		errService = inst.Update(value)
		if utils.CheckForServiceFetchError(w, errService.Err) {
			return
		}

		http.Redirect(w, r, value.OriginalURL, http.StatusPermanentRedirect)
	})
}

//ShorturlsStatistics get statistics for concrete short url
func ShorturlsStatistics(inst *services.ShortUrlService, userinst *services.UserService) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodGet {
			http.Error(w, http.StatusText(405), http.StatusBadRequest)
			return
		}

		user, errService, authOK := checkHTTPAuth(userinst, w, r)
		if !checkHTTPAuthError(w, user, errService, authOK) {
			return
		}

		shortURL := r.URL.Query().Get("src")
		if shortURL == "" {
			http.Error(w, "missing 'src' in query string", http.StatusBadRequest)
			return
		}

		value, errService := inst.Find(user.ID, shortURL)
		if utils.CheckForServiceFetchError(w, errService.Err) {
			return
		}

		w.WriteHeader(http.StatusOK)

		const maxValues = 20
		msg := "Statistics for URL: " + value.NewURL + "\n"
		fmt.Fprint(w, msg)

		for i, val := range value.Referers {
			if i == maxValues {
				break
			}
			fmt.Fprintf(w, "%d - %s\n", i+1, val)
		}
	})
}
