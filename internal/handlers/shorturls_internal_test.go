package handlers

import (
	"log"
	"testing"

	"bitbucket.org/Andrea23/gorestservice/internal/config"
	"bitbucket.org/Andrea23/gorestservice/internal/repository"
)

func TestShorturlsCreate(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	//userrepo, err := repository.CreateUsersRepo(repotype, config)
	_, _, err = repository.CreateUsersRepo(config)
	if err != nil {
		log.Fatalf("Cannot create Users repository: %s", err)
	}

	//usersrv := services.MakeUserService(userrepo)

	//userid := "Vasya"
	//passwd := "Qwerty"
	//email := "pupkin@dot.com"
}
