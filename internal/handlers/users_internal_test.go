package handlers

import (
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"bitbucket.org/Andrea23/gorestservice/internal/config"
	"bitbucket.org/Andrea23/gorestservice/internal/repository"
	"bitbucket.org/Andrea23/gorestservice/internal/services"
)

func TestUsersRegistration(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	_, userrepo, err := repository.CreateUsersRepo(config)
	if err != nil {
		log.Fatalf("Cannot create Users repository: %s", err)
	}

	usersrv := services.MakeUserService(userrepo)

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	form := url.Values{}
	form.Add("userid", userid)
	form.Add("email", email)
	form.Add("passwd", passwd)

	req, err := http.NewRequest(http.MethodPost, "/users/registration", strings.NewReader(form.Encode()))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	rr := httptest.NewRecorder()
	mux := http.NewServeMux()
	mux.Handle("/users/registration", UsersRegistration(&usersrv))

	mux.ServeHTTP(rr, req)
	resp := rr.Result()

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code %d", resp.StatusCode)
	}

	rr2 := httptest.NewRecorder()
	mux.ServeHTTP(rr2, req)
	resp = rr2.Result()

	if resp.StatusCode == http.StatusOK {
		t.Errorf("Unexpected status code %d", resp.StatusCode)
	}
}

func TestUsersAboutNoUser(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	_, userrepo, err := repository.CreateUsersRepo(config)
	if err != nil {
		log.Fatalf("Cannot create Users repository: %s", err)
	}

	usersrv := services.MakeUserService(userrepo)

	userid := "Vasya"
	passwd := "Qwerty"
	//email := "pupkin@dot.com"

	req, err := http.NewRequest(http.MethodGet, "/users/about", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.SetBasicAuth(userid, passwd)

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()

	mux := http.NewServeMux()
	mux.Handle("/users/about", UsersAbout(&usersrv))

	mux.ServeHTTP(rr, req)

	resp := rr.Result()
	if resp.StatusCode == http.StatusOK {
		t.Errorf("Unexpected status code %d", resp.StatusCode)
	}
}

func TestUsersAboutExistUser(t *testing.T) {

	config, err := config.ReadFromFile("../../configs/config_testing.json")
	if err != nil {
		t.Errorf("Cannot create Config from file: %s", err)
	}

	_, userrepo, err := repository.CreateUsersRepo(config)
	if err != nil {
		log.Fatalf("Cannot create Users repository: %s", err)
	}

	usersrv := services.MakeUserService(userrepo)

	userid := "Vasya"
	passwd := "Qwerty"
	email := "pupkin@dot.com"

	form := url.Values{}
	form.Add("userid", userid)
	form.Add("email", email)
	form.Add("passwd", passwd)

	req, err := http.NewRequest(http.MethodPost, "/users/registration", strings.NewReader(form.Encode()))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	rr := httptest.NewRecorder()
	mux := http.NewServeMux()
	mux.Handle("/users/registration", UsersRegistration(&usersrv))

	mux.ServeHTTP(rr, req)
	resp := rr.Result()

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code %d", resp.StatusCode)
	}

	req, err = http.NewRequest(http.MethodGet, "/users/about", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.SetBasicAuth(userid, passwd)

	// // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	// rr = httptest.NewRecorder()

	mux.Handle("/users/about", UsersAbout(&usersrv))
	mux.ServeHTTP(rr, req)

	resp = rr.Result()
	if resp.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code %d", resp.StatusCode)
	}
}
