package handlers

import (
	"fmt"
	"net/http"

	"bitbucket.org/Andrea23/gorestservice/internal/models"
	"bitbucket.org/Andrea23/gorestservice/internal/services"
	"bitbucket.org/Andrea23/gorestservice/internal/utils"
)

func checkHTTPAuth(inst *services.UserService, w http.ResponseWriter, r *http.Request) (models.User, models.ServiceError, bool) {

	w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)

	user, passwd, authOK := r.BasicAuth()
	if !authOK {
		return models.User{}, models.ServiceError{}, authOK
	}

	userFromSrv, errService := inst.User(user)
	if errService.Err != nil {
		return userFromSrv, errService, authOK
	}

	errService = inst.CheckBasicAuth(userFromSrv, user, passwd)
	return userFromSrv, errService, authOK
}

func checkHTTPAuthError(w http.ResponseWriter, user models.User, errService models.ServiceError, authOK bool) bool {

	if !authOK {
		http.Error(w, "Not authorized", http.StatusUnauthorized)
		return false
	}

	if !checkHTTPError(w, errService) {
		return false
	}

	w.WriteHeader(http.StatusOK)
	return true
}

func checkHTTPError(w http.ResponseWriter, errService models.ServiceError) bool {

	if errService.Err != nil {

		status := http.StatusInternalServerError
		if errService.Status == models.StatusServiceUnauthorized {
			status = http.StatusUnauthorized
		}

		http.Error(w, errService.Err.Error(), status)
		return false
	}

	return true
}

// UsersRegistration registrate new user in service
func UsersRegistration(inst *services.UserService) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// application/x-www-form-urlencoded
		if r.Method != http.MethodPost {
			http.Error(w, http.StatusText(405), http.StatusBadRequest)
			return
		}

		params := []string{"userid", "email", "passwd"}
		if utils.CheckForReqPostParams(w, r, params) {
			return
		}

		userid := r.Form.Get("userid")
		email := r.Form.Get("email")
		passwd := r.Form.Get("passwd")

		passwdHash, err := utils.HashPassword(passwd)

		if err != nil {
			http.Error(w, "Something wrong...", http.StatusInternalServerError)
			return
		}

		errService := inst.Registration(userid, email, passwdHash)
		if !checkHTTPError(w, errService) {
			return
		}

		w.WriteHeader(http.StatusOK)
	})
}

// UsersAbout get info about current user
func UsersAbout(inst *services.UserService) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodGet {
			http.Error(w, http.StatusText(405), http.StatusBadRequest)
			return
		}

		user, errService, authOK := checkHTTPAuth(inst, w, r)
		if !checkHTTPAuthError(w, user, errService, authOK) {
			return
		}

		result, errService := inst.PrintUser(user)
		if !checkHTTPError(w, errService) {
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w, result)
	})
}
